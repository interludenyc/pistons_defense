define(
    [
        "./Utils",
        "../node_modules/eventemitter3/index",
        'js/LevelLogic',
        'js/gui/SuperPower',
        'text!templates/score.html'
    ],

    function (Utils,
              EventEmitter,
              LevelLogic,
              SuperPowerGui,
              ScoreTemplate
    ) {

        var useKeyboard = !Utils.isMobile() && !("forcetouch" in Utils.getUrlVars());

        var LevelGui = function(player, levelLogic){
            this.levelLogic = levelLogic;
            this.player = player;

            var eventEmitter = new EventEmitter();
            this.emit = eventEmitter.emit;
            this.on = eventEmitter.on;
            this.once = eventEmitter.once;
            this.removeListener = eventEmitter.removeListener;

            initDom.call(this);
            this.hide(); //todo should be hidden in default

            levelLogic.on("attachActionSequence", onAttachActionSequence, this);
            levelLogic.on("actionSequenceStart", onActionSequenceStart, this);
            levelLogic.on("actionSequenceComplete", onActionSequenceEnd, this);
            levelLogic.on("comboUpdate", onComboUpdate, this);
        };

        LevelGui.prototype.constructor = LevelGui;

        LevelGui.showGesturePrompt = function(actionItem){

        };

        LevelGui.prototype.hide = function(){
            this.hidePrompts();

            this.superPowerGui.canvasEl.style.opacity = 0;
            this.scoreContainer.style.opacity = 0;
        };

        LevelGui.prototype.hidePrompts = function() {
            if (useKeyboard){
                this.key1prompt.style.opacity = 0;
                this.key2prompt.style.opacity = 0;
            } else {
                for (var imageName in this.gestureImages){
                    this.gestureImages[imageName].style.opacity = 0;
                }
            }
        };

        LevelGui.prototype.show = function(){
            this.superPowerGui.canvasEl.style.opacity = 1;
            this.scoreContainer.style.opacity = 1;
        };

        LevelGui.prototype.updateKidScore = function(score){
            this.scoreContainer.querySelector(".levelScore__kid .levelScore__number").innerHTML = score;
        };

        LevelGui.prototype.updateOpponentScore = function(score){
            this.scoreContainer.querySelector(".levelScore__opponent .levelScore__number").innerHTML = score;
        };

        LevelGui.prototype.dischargeSuperPower = function(){
            this.superPowerGui.discharge();
        };


        //called when a level is initiated
        var onAttachActionSequence = function(actionSequence){

            if (useKeyboard){

            }
            else {
                var imgSrc, gestureImage;

                var gestureName = getGestureName(actionSequence);

                if (this.gestureImages[gestureName]) {
                    gestureImage = this.gestureImages[gestureName];
                }
                else {
                    gestureImage = this.gestureImages[gestureName] = document.createElement("div");
                    gestureImage.classList.add("gestureImage");

                    if (actionSequence.type == "swipe") {
                        imgSrc = "resources/images/gesture_down.png";
                    }
                    else if (actionSequence.type == "stroke") {
                        switch (actionSequence.shape) {
                            case "elbow":
                                imgSrc = "resources/images/gesture_elbow.png";
                                break;
                            case "n":
                                imgSrc = "resources/images/gesture_n.png";
                                break;

                        }
                    }

                    gestureImage.style.backgroundImage = "url(" + imgSrc + ")";
                    this.gestureContainer.appendChild(gestureImage);
                }
            }
        };


        //called when an action sequence is started for the player
        var onActionSequenceStart = function(actionSequence){

            if (useKeyboard) {
                this.key1prompt.style.opacity = 1;
                this.key2prompt.style.opacity = 1;

                var gestureName = this.levelLogic.getGestureName(actionSequence);
                if (LevelLogic.gestureToKeySequence[gestureName]) {
                    var keySequence = LevelLogic.gestureToKeySequence[gestureName].split(" ");
                }
                else{
                    console.error("no key sequence for", gestureName);
                }
                this.key1prompt.innerHTML = keyDictionary[keySequence[0]];
                this.key2prompt.innerHTML = keyDictionary[keySequence[1]];
            }
            else {
                var gestureName = getGestureName(actionSequence);
                var gestureImage = this.gestureImages[gestureName];

                this.gestureContainer.classList.remove("cw", "cww", "cwww");
                if (actionSequence.type == "swipe") {
                    switch (actionSequence.direction) {
                        case "left":
                            this.gestureContainer.classList.add("cw");
                            break;
                        case "up":
                            this.gestureContainer.classList.add("cww");
                            break;
                        case "right":
                            this.gestureContainer.classList.add("cwww");
                            break;
                    }
                }
                else if (actionSequence.type == "stroke") {
                    if (actionSequence.rotate && actionSequence.rotate != "") {
                        //rotation is defined
                        this.gestureContainer.classList.add(actionSequence.rotate);
                    }
                }
                gestureImage.style.opacity = 1;
            }

        };

        var onActionSequenceEnd = function(actionSequence, playerWon){
            if (useKeyboard) {
                this.key1prompt.style.opacity = 0;
                this.key2prompt.style.opacity = 0;
            } else {
                var gestureName = getGestureName(actionSequence);
                var gestureImage = this.gestureImages[gestureName];

                gestureImage.style.opacity = 0;
            }
        };


        var onComboUpdate = function(comboGauge, superPowerCharged){
            this.superPowerGui.addActiveSegment(comboGauge - 1);
            if (superPowerCharged){
                this.superPowerGui.superPowerCharged();
            }
        };

        //private functions

        function initDom(){
            var that = this;

            this.guiContainer = document.createElement("div");
            this.player.overlays.add("levelGui", this.guiContainer, {pointerEvents: false});

            //superpower
            this.superPowerGui = new SuperPowerGui(null, {
                onClick: function(){
                    that.emit("clickedSuperPower");
                }
            });
            this.superPowerGui.canvasEl.classList.add("superPower");
            this.guiContainer.appendChild(this.superPowerGui.canvasEl);


            //score
            this.scoreContainer = Utils.htmlToElement(ScoreTemplate);
            this.guiContainer.appendChild(this.scoreContainer);

            //gesture prompts
            this.gestureContainer = document.createElement("div");
            this.gestureContainer.classList.add("gestureContainer");
            this.guiContainer.appendChild(this.gestureContainer);

            if (useKeyboard) {
                //keyboard keys
                this.key1prompt = document.createElement("div");
                this.key1prompt.classList.add("keyPrompt");

                this.key2prompt = document.createElement("div");
                this.key2prompt.classList.add("keyPrompt");

                this.gestureContainer.appendChild(this.key1prompt);
                this.gestureContainer.appendChild(this.key2prompt);

            }
            else{
                //shape & arrow gestures
                this.gestureContainer.classList.add("gestureImageContainer");
                this.gestureImages = {};
            }

        };


        //todo remove this function, use the one from LevelLogic
        function getGestureName(actionSequence){
            if (actionSequence.type == "swipe") {
                return "down";
            }
            else if (actionSequence.type == "stroke"){
                return actionSequence.shape;
            }
        }


        var keyDictionary = {
            "up": "▲",
            "down": "▼",
            "left": "◀",
            "right": "▶",
            "space": "sp",
            "enter": "↵"
        };

        return LevelGui;
    }
);