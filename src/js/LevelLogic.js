define(
    [
        "./Utils",
        "../node_modules/eventemitter3/index",
        "./KeyboardHandler",
        "./SwipeRenderer",
        './Strokeable',
        './Swipeable'
    ],

    function (Utils, EventEmitter,
              KeyboardHandler, SwipeRenderer, Strokeable, Swipeable) {
        var player, strokeable, swipeable;

        var useKeyboard = !Utils.isMobile() && !("forcetouch" in Utils.getUrlVars());

        var gestureCatcher = document.createElement('div');
        // gestureCatcher.style.backgroundColor= "rgba(255, 0, 0, 0)";
        gestureCatcher.style.width = "100%";
        gestureCatcher.style.height = "100%";
        gestureCatcher.style.transition = "all 0.3s ease-out";

        var LevelLogic = function(){
            var eventEmitter = new EventEmitter();
            this.emit = eventEmitter.emit;
            this.on = eventEmitter.on;
            this.once = eventEmitter.once;
            this.removeListener = eventEmitter.removeListener;

            this.maxComboGauge = 16;
            this.comboGauge = 0;
            this.currentComboLength = 0;
        };

        LevelLogic.prototype.constructor = LevelLogic;

        LevelLogic.gestureToKeySequence = {
            swiperight: "right right",
            swipeleft: "left left",
            swipeup: "up up",
            swipedown: "down down",
            //todo rotated elbows, other shapes
            elbow: "down left",
            elbowcw: "left up",
            elbowcww: "up right",
            elbowcwww: "right down",

            n: "down space"
        };

        LevelLogic.prototype.init = function(delegate){
            var that = this;

            player = delegate.player;
            player.overlays.add("gestureCatcher", gestureCatcher, {pointerEvents: false});
            strokeable = new Strokeable(this, player, gestureCatcher);
            swipeable = new Swipeable(this, player, gestureCatcher);

            SwipeRenderer.init(gestureCatcher, player);

        };

        LevelLogic.prototype.getGestureName = function(actionSequence){
            if (actionSequence.type == "swipe") {
                return "swipe"+actionSequence.direction;
            }
            else if (actionSequence.type == "stroke"){
                return actionSequence.shape + actionSequence.rotate;
            }
        };

        LevelLogic.prototype.attachActionSequence = function(node, actionSequence, inputOptions){
            var that = this;

            var sequenceOptions = Utils.extend({
                startTime: Math.max(0, node.duration - 1.2 - 0.4),
                endTime: node.duration
            }, inputOptions);

            //actionSequence might be a single actionItem
            if (Object.prototype.toString.call( actionSequence ) != '[object Array]'){
                var actionItem = actionSequence;
            }

            this.emit("attachActionSequence", actionSequence);


            var actionPromise = new Promise(function(resolve, reject) {
                //input start time
                node.once("timeupdate:" + sequenceOptions.startTime, function () {
                    node.playerSuccess = false;
                    console.log("action sequence start", node.id);

                    var params = Utils.extend(sequenceOptions, actionItem);
                    gestureCatcher.style.pointerEvents = "all";

                    //todo: put into own module
                    if (!useKeyboard) {
                        SwipeRenderer.enableDrawing();
                        var hammerInstance;

                        // player.pause(); debug

                        if (actionItem.type == "stroke") {
                            hammerInstance = strokeable.add(params, resolve).hammerInstance;
                        }
                        else if (actionItem.type == "swipe")
                        {
                            hammerInstance = swipeable.add(params, resolve).hammerInstance;
                        }

                        SwipeRenderer.attacheHammerInstance(hammerInstance);
                    }

                    //show gui prompt
                    that.emit("actionSequenceStart", actionSequence, resolve);
                });


                //ellapsed time has ended
                node.once("timeupdate:" + sequenceOptions.endTime, function() {
                    that.emit("actionSequenceEndTime", actionSequence, resolve);

                    // console.log("action sequence end", node.id, node.playerSuccess);

                    //user might have already won and moved on to a different node
                    if (node.playerSuccess == false){
                        resolve(false);
                    }
                });


                //user either failed or succeeded
                }).then(function(success) {
                    node.playerSuccess = success;
                    //TODO I would much rather do a generic "off" here to remove the handler,
                    //rather than to tie this to the node but alas no such function :(

                    //handler score & combo
                    if (success){
                        updateCombo.call(that);
                    }
                    else{
                        this.currentCombo = 0;
                    }

                    gestureCatcher.style.pointerEvents = "none";

                    //todo: put into own module
                    if (!useKeyboard) {
                        SwipeRenderer.disableDrawing();
                        SwipeRenderer.clear();
                    }

                    that.emit("actionSequenceComplete", actionSequence, success);

                    return success;
                });

            return actionPromise;
        };

        function updateCombo(){
            var superPowerPreviouslyCharged = this.comboGauge == this.maxComboGauge;
            if (superPowerPreviouslyCharged) return;

            this.currentComboLength++;
            // console.log(this.currentComboLength);

            if (this.currentComboLength == 1){
                this.comboGauge += 1;
            }
            else{
                this.comboGauge += this.maxComboGauge * (1 / (this.maxComboGauge / 2) + (1 / this.maxComboGauge) * this.currentComboLength);
            }

            this.comboGauge = Math.min(this.maxComboGauge, this.comboGauge);

            var superPowerCharged = this.comboGauge == this.maxComboGauge;
            this.emit("comboUpdate", this.comboGauge, superPowerCharged);
        }



        return LevelLogic;

    }
);