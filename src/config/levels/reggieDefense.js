define([], function () {

    var levelConfig = {
        initialStepId: "R_Df_01_Begin_01",
        steps: [
            {
                id: "R_Df_01_Begin_01",
                success: [
                    { id: "R_Df_01_Goto02_PREP"},
                    { id: "R_Df_01_Goto05_PREP"},
                    { id: "R_Df_01_3ptshot_PREP"}
                ]
            },
            {
                id: "R_Df_01_Goto02_PREP",
                success: [
                    { id: "R_Df_0102_3ptshot_PREP"},
                    { id: "R_Df_0102_Goto06_PREP" }
                ],
                failure: "R_Df_01_Goto02_SCORE",
                super: "R_Df_01_Goto02_STEAL",
                actionSequence:  {
                    type: "swipe",
                    direction: "left"
                }
            },
            {
                id: "R_Df_01_Goto05_PREP",
                success: [
                    { id: "R_Df_0105_2ptshot_PREP"},
                    { id: "R_Df_0105_Goto06_PREP" },
                    { id: "R_Df_0105_Goto07_PREP" }
                ],
                failure: "R_Df_01_Goto05_SCORE",
                super: "R_Df_01_Goto05_STEAL",
                actionSequence:  {
                    type: "stroke",
                    shape: "elbow",
                    rotate: "cw"
                }
            },
            {
                id: "R_Df_01_3ptshot_PREP",
                success: [
                    { id: "R_Df_01_3ptshot_SSCORE" },
                    { id: "R_Df_01_3ptshot_MISS" }
                ],
                failure: "R_Df_01_3ptshot_FSCORE",
                super: "R_Df_01_3ptshot_BLOCK",
                actionSequence:  {
                    type: "stroke",
                    shape: "elbow",
                    rotate: "cw"
                }
            },
            {
                id: "R_Df_0102_Goto06_PREP",
                success: [
                    { id: "R_Df_0206_2ptshot_PREP" },
                ],
                super: "R_Df_0102_Goto06_STEAL",
                failure: "R_Df_0102_Goto06_SCORE",

                actionSequence:  {
                    type: "stroke",
                    shape: "elbow",
                    rotate: "cww"
                }
            },
            {
                id: "R_Df_0102_3ptshot_PREP",
                success: [
                    { id: "R_Df_0102_3ptshot_SSCORE" },
                    { id: "R_Df_0102_3ptshot_MISS" }
                ],
                super: "R_Df_0102_3ptshot_BLOCK",
                failure: "R_Df_0102_3ptshot_FSCORE",

                actionSequence:  {
                    type: "swipe",
                    direction: "up"
                }
            },
            {
                id: "R_Df_0105_Goto06_PREP",
                success: [
                    { id: "R_Df_0506_2ptshot_PREP" }
                ],
                super: "R_Df_0105_Goto06_STEAL",
                failure: "R_Df_0105_Goto06_SCORE",

                actionSequence:  {
                    type: "swipe",
                    direction: "down"
                }
            },
            {
                id: "R_Df_0105_Goto07_PREP",
                success: [
                    { id: "R_Df_0507_2ptshot_PREP" }
                ],
                super: "R_Df_0105_Goto07_STEAL",
                failure: "R_Df_0105_Goto07_SCORE",

                actionSequence:  {
                    type: "stroke",
                    shape: "elbow",
                    rotate: "cw" // TODO should be reverse elbow, cw
                }
            },
            {
                id: "R_Df_0105_2ptshot_PREP",
                success: [
                    { id: "R_Df_0105_2ptshot_SSCORE" },
                    { id: "R_Df_0105_2ptshot_MISS" }
                ],
                super: "R_Df_0105_2ptshot_BLOCK",
                failure: "R_Df_0105_2ptshot_FSCORE",

                actionSequence:  {
                    type: "swipe",
                    direction: "up"
                }
            },
            {
                id: "R_Df_0206_2ptshot_PREP",
                success: [
                    { id: "R_Df_0206_2ptshot_SSCORE"},
                    { id: "R_Df_0206_2ptshot_MISS" },
                ],
                super: "R_Df_0206_2ptshot_BLOCK",
                failure: "R_Df_0206_2ptshot_FSCORE",
                actionSequence:  {
                    type: "stroke",
                    shape: "elbow",
                    rotate: "cww"
                }
            },
            {
                id: "R_Df_0506_2ptshot_PREP",
                success: [
                    { id: "R_Df_0506_2ptshot_SSCORE" },
                    { id: "R_Df_0506_2ptshot_MISS" },
                ],
                super: "R_Df_0506_2ptshot_BLOCK",
                failure: "R_Df_0506_2ptshot_FSCORE",
                actionSequence:  {
                    type: "stroke",
                    shape: "elbow",
                    rotate: "cww"
                }
            },
            {
                id: "R_Df_0507_2ptshot_PREP",
                success: [
                    { id: "R_Df_0507_2ptshot_SSCORE" },
                    { id: "R_Df_0507_2ptshot_MISS" },
                ],
                super: "R_Df_0507_2ptshot_BLOCK",
                failure: "R_Df_0507_2ptshot_FSCORE",
                actionSequence:  {
                    type: "stroke",
                    shape: "elbow",
                    rotate: "cww"
                }
            },

            { id: "R_Df_01_Goto02_STEAL" },
            { id: "R_Df_01_Goto02_SCORE", opponentScore: 1},
            { id: "R_Df_01_Goto05_STEAL" },
            { id: "R_Df_01_Goto05_SCORE", opponentScore: 1 },
            { id: "R_Df_01_3ptshot_SSCORE", opponentScore: 2 },
            { id: "R_Df_01_3ptshot_MISS" },
            { id: "R_Df_01_3ptshot_BLOCK" },
            { id: "R_Df_01_3ptshot_FSCORE", opponentScore: 2 },
            { id: "R_Df_0102_Goto06_STEAL" },
            { id: "R_Df_0102_Goto06_SCORE", opponentScore: 1  },
            { id: "R_Df_0102_3ptshot_SSCORE", opponentScore: 2  },
            { id: "R_Df_0102_3ptshot_MISS" },
            { id: "R_Df_0102_3ptshot_BLOCK" },
            { id: "R_Df_0102_3ptshot_FSCORE", opponentScore: 2 },
            { id: "R_Df_0105_Goto06_STEAL" },
            { id: "R_Df_0105_Goto06_SCORE", opponentScore: 1 },
            { id: "R_Df_0105_Goto07_STEAL" },
            { id: "R_Df_0105_Goto07_SCORE" , opponentScore: 1},
            { id: "R_Df_0105_2ptshot_SSCORE", opponentScore: 1 },
            { id: "R_Df_0105_2ptshot_MISS" },
            { id: "R_Df_0105_2ptshot_BLOCK" },
            { id: "R_Df_0105_2ptshot_FSCORE", opponentScore: 1  },
            { id: "R_Df_0206_2ptshot_SSCORE", opponentScore: 1  },
            { id: "R_Df_0206_2ptshot_MISS" },
            { id: "R_Df_0206_2ptshot_BLOCK" },
            { id: "R_Df_0206_2ptshot_FSCORE", opponentScore: 1  },
            { id: "R_Df_0506_2ptshot_SSCORE", opponentScore: 1  },
            { id: "R_Df_0506_2ptshot_MISS" },
            { id: "R_Df_0506_2ptshot_BLOCK" },
            { id: "R_Df_0506_2ptshot_FSCORE", opponentScore: 1  },
            { id: "R_Df_0507_2ptshot_SSCORE", opponentScore: 1  },
            { id: "R_Df_0507_2ptshot_MISS" },
            { id: "R_Df_0507_2ptshot_BLOCK" },
            { id: "R_Df_0507_2ptshot_FSCORE", opponentScore: 1  },

        ]
    };

    return levelConfig;
});