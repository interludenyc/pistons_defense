define([
        '../../node_modules/hammerjs/hammer',
        "./Utils"
    ],

    function (Hammer, Utils) {

        var Swipeable = function(levelLogic, player, gestureCatcher){
            this.levelLogic = levelLogic;
            this.player = player;
            this.gestureCatcher = gestureCatcher;
        };

        Swipeable.prototype.add = function(options, resolve){
            var that = this;

            this.mc = new Hammer(this.gestureCatcher, {
                // touchAction: "none"
                // recognizers: [[Hammer.Swipe,{}]]
            });

            switch (options.direction){
                case "up":
                    this.mc.get('swipe').set({ direction: Hammer.DIRECTION_UP });
                    break;
                case "left":
                    this.mc.get('swipe').set({ direction: Hammer.DIRECTION_LEFT });
                    break;
                case "down":
                    this.mc.get('swipe').set({ direction: Hammer.DIRECTION_DOWN });
                    break;
                case "right":
                    this.mc.get('swipe').set({ direction: Hammer.DIRECTION_RIGHT });
                    break;

            }


            this.levelLogic.on("actionSequenceEndTime", function(){
                that.destroy();
            });


            that.mc.on("swipe", function(ev){
                //user "won"
                this.playerSuccess = true;

                resolve(true);
                that.destroy();
            });


            return{
                hammerInstance: this.mc
            }
        };

        Swipeable.prototype.destroy = function(){
            this.mc.destroy();
        };


        return Swipeable;
    }
);