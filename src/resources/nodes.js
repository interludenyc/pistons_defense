define([
    'resources/ivds'
    ], function(ivds) {
        var nodes = [
            {
                id:'pistons_defense.Intro_Flipped_082616',
                source:ivds.vid_Intro_Flipped_082616,
                data: {}
            },
            {
                id:'pistons_defense.Outro_Tests',
                source:ivds.vid_Outro_Tests,
                data: {}
            },
            {
                id:'pistons_defense.REGGIE',
                source:ivds.vid_REGGIE,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0102_3ptshot_BLOCK',
                source:ivds.vid_R_Df_0102_3ptshot_BLOCK,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0102_3ptshot_FSCORE',
                source:ivds.vid_R_Df_0102_3ptshot_FSCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0102_3ptshot_MISS',
                source:ivds.vid_R_Df_0102_3ptshot_MISS,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0102_3ptshot_PREP',
                source:ivds.vid_R_Df_0102_3ptshot_PREP,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0102_3ptshot_SSCORE',
                source:ivds.vid_R_Df_0102_3ptshot_SSCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0102_Goto06_PREP',
                source:ivds.vid_R_Df_0102_Goto06_PREP,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0102_Goto06_SCORE',
                source:ivds.vid_R_Df_0102_Goto06_SCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0102_Goto06_STEAL',
                source:ivds.vid_R_Df_0102_Goto06_STEAL,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0105_2ptshot_BLOCK',
                source:ivds.vid_R_Df_0105_2ptshot_BLOCK,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0105_2ptshot_FSCORE',
                source:ivds.vid_R_Df_0105_2ptshot_FSCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0105_2ptshot_MISS',
                source:ivds.vid_R_Df_0105_2ptshot_MISS,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0105_2ptshot_PREP',
                source:ivds.vid_R_Df_0105_2ptshot_PREP,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0105_2ptshot_SSCORE',
                source:ivds.vid_R_Df_0105_2ptshot_SSCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0105_Goto06_PREP',
                source:ivds.vid_R_Df_0105_Goto06_PREP,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0105_Goto06_SCORE',
                source:ivds.vid_R_Df_0105_Goto06_SCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0105_Goto06_STEAL',
                source:ivds.vid_R_Df_0105_Goto06_STEAL,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0105_Goto07_PREP',
                source:ivds.vid_R_Df_0105_Goto07_PREP,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0105_Goto07_SCORE',
                source:ivds.vid_R_Df_0105_Goto07_SCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0105_Goto07_STEAL',
                source:ivds.vid_R_Df_0105_Goto07_STEAL,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_01_3ptshot_BLOCK',
                source:ivds.vid_R_Df_01_3ptshot_BLOCK,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_01_3ptshot_FSCORE',
                source:ivds.vid_R_Df_01_3ptshot_FSCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_01_3ptshot_MISS',
                source:ivds.vid_R_Df_01_3ptshot_MISS,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_01_3ptshot_PREP',
                source:ivds.vid_R_Df_01_3ptshot_PREP,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_01_3ptshot_SSCORE',
                source:ivds.vid_R_Df_01_3ptshot_SSCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_01_Begin_01',
                source:ivds.vid_R_Df_01_Begin_01,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_01_Begin_02',
                source:ivds.vid_R_Df_01_Begin_02,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_01_Goto02_PREP',
                source:ivds.vid_R_Df_01_Goto02_PREP,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_01_Goto02_SCORE',
                source:ivds.vid_R_Df_01_Goto02_SCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_01_Goto02_STEAL',
                source:ivds.vid_R_Df_01_Goto02_STEAL,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_01_Goto05_PREP',
                source:ivds.vid_R_Df_01_Goto05_PREP,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_01_Goto05_SCORE',
                source:ivds.vid_R_Df_01_Goto05_SCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_01_Goto05_STEAL',
                source:ivds.vid_R_Df_01_Goto05_STEAL,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0206_2ptshot_BLOCK',
                source:ivds.vid_R_Df_0206_2ptshot_BLOCK,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0206_2ptshot_FSCORE',
                source:ivds.vid_R_Df_0206_2ptshot_FSCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0206_2ptshot_MISS',
                source:ivds.vid_R_Df_0206_2ptshot_MISS,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0206_2ptshot_PREP',
                source:ivds.vid_R_Df_0206_2ptshot_PREP,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0206_2ptshot_SSCORE',
                source:ivds.vid_R_Df_0206_2ptshot_SSCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0506_2ptshot_BLOCK',
                source:ivds.vid_R_Df_0506_2ptshot_BLOCK,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0506_2ptshot_FSCORE',
                source:ivds.vid_R_Df_0506_2ptshot_FSCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0506_2ptshot_MISS',
                source:ivds.vid_R_Df_0506_2ptshot_MISS,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0506_2ptshot_PREP',
                source:ivds.vid_R_Df_0506_2ptshot_PREP,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0506_2ptshot_SSCORE',
                source:ivds.vid_R_Df_0506_2ptshot_SSCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0507_2ptshot_BLOCK',
                source:ivds.vid_R_Df_0507_2ptshot_BLOCK,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0507_2ptshot_FSCORE',
                source:ivds.vid_R_Df_0507_2ptshot_FSCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0507_2ptshot_MISS',
                source:ivds.vid_R_Df_0507_2ptshot_MISS,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0507_2ptshot_PREP',
                source:ivds.vid_R_Df_0507_2ptshot_PREP,
                data: {}
            },
            {
                id:'pistons_defense.R_Df_0507_2ptshot_SSCORE',
                source:ivds.vid_R_Df_0507_2ptshot_SSCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_0103_3ptshot_BLOCK',
                source:ivds.vid_R_Of_0103_3ptshot_BLOCK,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_0103_3ptshot_PREP',
                source:ivds.vid_R_Of_0103_3ptshot_PREP,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_0103_3ptshot_SCORE',
                source:ivds.vid_R_Of_0103_3ptshot_SCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_0103_Goto07_PREP',
                source:ivds.vid_R_Of_0103_Goto07_PREP,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_0103_Goto07_STEAL',
                source:ivds.vid_R_Of_0103_Goto07_STEAL,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_0104_2ptshot_BLOCK',
                source:ivds.vid_R_Of_0104_2ptshot_BLOCK,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_0104_2ptshot_PREP',
                source:ivds.vid_R_Of_0104_2ptshot_PREP,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_0104_2ptshot_SCORE',
                source:ivds.vid_R_Of_0104_2ptshot_SCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_0104_Goto06_MISHANDLE',
                source:ivds.vid_R_Of_0104_Goto06_MISHANDLE,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_0104_Goto06_PREP',
                source:ivds.vid_R_Of_0104_Goto06_PREP,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_0104_Layup_BLOCK',
                source:ivds.vid_R_Of_0104_Layup_BLOCK,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_0104_Layup_PREP',
                source:ivds.vid_R_Of_0104_Layup_PREP,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_0104_Layup_SCORE',
                source:ivds.vid_R_Of_0104_Layup_SCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_01_Begin_01',
                source:ivds.vid_R_Of_01_Begin_01,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_01_Begin_02',
                source:ivds.vid_R_Of_01_Begin_02,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_01_Begin_03',
                source:ivds.vid_R_Of_01_Begin_03,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_01_Begin_04',
                source:ivds.vid_R_Of_01_Begin_04,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_01_Begin_05',
                source:ivds.vid_R_Of_01_Begin_05,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_01_Goto03_MISHANDLE',
                source:ivds.vid_R_Of_01_Goto03_MISHANDLE,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_01_Goto03_PREP',
                source:ivds.vid_R_Of_01_Goto03_PREP,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_01_Goto04_MISHANDLE',
                source:ivds.vid_R_Of_01_Goto04_MISHANDLE,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_01_Goto04_PREP',
                source:ivds.vid_R_Of_01_Goto04_PREP,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_0307_Layup_BLOCK',
                source:ivds.vid_R_Of_0307_Layup_BLOCK,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_0307_Layup_PREP',
                source:ivds.vid_R_Of_0307_Layup_PREP,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_0307_Layup_SCORE',
                source:ivds.vid_R_Of_0307_Layup_SCORE,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_0406_Layup_BLOCK',
                source:ivds.vid_R_Of_0406_Layup_BLOCK,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_0406_Layup_PREP',
                source:ivds.vid_R_Of_0406_Layup_PREP,
                data: {}
            },
            {
                id:'pistons_defense.R_Of_0406_Layup_SCORE',
                source:ivds.vid_R_Of_0406_Layup_SCORE,
                data: {}
            }];
    return nodes;
    }
);