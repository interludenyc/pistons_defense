define(
    [
        "./Utils",
        "../../node_modules/mousetrap/mousetrap"
    ],

    function (Utils, Mousetrap) {
        var LevelLogic;

        function KeyboardHandler(levelLogic){
            this.levelLogic = levelLogic;

            LevelLogic = levelLogic.constructor;

            Mousetrap.bind('up', onPressedUp.bind(this));
            Mousetrap.bind('left', onPressedLeft.bind(this));
            Mousetrap.bind('down', onPressedDown.bind(this));
            Mousetrap.bind('right', onPressedRight.bind(this));
            Mousetrap.bind('space', onPressedSpace.bind(this));

            levelLogic.on("attachActionSequence", onAttachActionSequence, this);
            levelLogic.on("actionSequenceStart", onActionSequenceStart, this);
            levelLogic.on("actionSequenceComplete", onActionSequenceEnd, this);
        };

        KeyboardHandler.prototype.constructor = KeyboardHandler;

        var onAttachActionSequence = function(actionSequence) {

        };

        var onActionSequenceStart = function(actionSequence, resolveFunction){
            var gestureName = this.levelLogic.getGestureName(actionSequence);
            this.sequenceIndex = 0;
            this.currentSequence = LevelLogic.gestureToKeySequence[gestureName].split(" ");
            this.resolveFunction = resolveFunction;
        };

        var onActionSequenceEnd = function(actionSequence, playerWon) {
        };


        var onPressedUp = function(){
            checkCurrentSequence.call(this, "up");
            return false;
        };

        var onPressedLeft = function(){
            checkCurrentSequence.call(this, "left");
            return false;
        };

        var onPressedRight = function(){
            checkCurrentSequence.call(this, "right");
            return false;
        };

        var onPressedDown = function(){
            checkCurrentSequence.call(this, "down");
            return false;
        };

        var onPressedSpace = function(){
            checkCurrentSequence.call(this, "space");
            return false;
        };


        function checkCurrentSequence(keyName){
            if (this.currentSequence){
                if (this.currentSequence[this.sequenceIndex]==keyName){
                    //pressed proper key

                    //finished the combo
                    if (this.sequenceIndex == this.currentSequence.length-1){
                        this.resolveFunction(true);
                    }
                    else{
                        this.sequenceIndex++;
                    }

                }
                else{
                    //wrong key, reset the sequence
                    this.sequenceIndex = 0;
                }
            }
        }


        return KeyboardHandler;
    });