define(
    [
        "./../Utils",
         "https://cdn.rawgit.com/flozz/StackBlur/master/dist/stackblur.js",
        // "./../../../node_modules/stackblur-canvas/dist/stackblur"
    ],

    function (Utils, StackBlur) {

        var Circle = function(options){
            var that = this;

            this.options = Utils.extend({
                centerX: 0,
                centerY: 0,
                radius: 100,
                alpha: 1,
                fillStyle: null,
                strokeStyle: null,
                lineWidth: 1,
                shadowBlur: null,
                shadowColor: null,
                shadowOffsetX: 0,
                shadowOffsetY: 0,
                blurRadius: 0
            }, options);

            for (var prop in this.options){
                this[prop] = this.options[prop];
            }

            this.tempCanvas = document.createElement("canvas");
            this.tempCanvas.width = this.ctx.canvas.width;
            this.tempCanvas.height = this.ctx.canvas.height;
            this.tempCtx = this.tempCanvas.getContext("2d");
        };
        Circle.prototype.constructor = Circle;

        Circle.prototype.step = function(){
            var ctx = this.tempCtx;
            ctx.save();

            ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);

            ctx.beginPath();
            ctx.globalAlpha = this.alpha;
            // ctx.globalCompositeOperation = "lighter";
            ctx.fillStyle = this.fillStyle;
            ctx.strokeStyle = this.strokeStyle;
            ctx.lineWidth = this.lineWidth;
            ctx.shadowBlur = this.shadowBlur;
            ctx.shadowColor = this.shadowColor;
            ctx.shadowOffsetX = this.shadowOffsetX;
            ctx.shadowOffsetY = this.shadowOffsetY;
            ctx.arc(this.centerX,
                this.centerY,
                this.radius,
                0,
                Math.PI *2
            );

            if (this.fillStyle) ctx.fill();
            if (this.lineWidth>0) ctx.stroke();


            StackBlur.canvasRGBA(this.tempCanvas,
                0,
                0,
                this.tempCanvas.width,
                this.tempCanvas.height,
                this.blurRadius);

            this.ctx.drawImage(ctx.canvas, 0, 0);

            ctx.restore();
        };

        return Circle;

    });