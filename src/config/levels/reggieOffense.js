define([], function () {

        var levelConfig = {
            initialStepId: "R_Of_01_Begin_01",
            steps: [
                {
                    id: "R_Of_01_Begin_01",
                    success: [
                        { id: "R_Of_01_Goto03_PREP"},
                        { id: "R_Of_01_Goto04_PREP"}
                    ]
                },
                {
                    id: "R_Of_01_Goto03_PREP",
                    success: [
                        { id: "R_Of_0103_Goto07_PREP"},
                        { id: "R_Of_0103_3ptshot_PREP" }
                    ],
                    failure: "R_Of_01_Goto03_MISHANDLE",
                    actionSequence:  {
                            type: "swipe",
                            "direction": "right"
                        }
                },
                {
                    id: "R_Of_01_Goto04_PREP",
                    success: [
                        { id: "R_Of_0104_Goto06_PREP"},
                        { id: "R_Of_0104_Layup_PREP" },
                        { id: "R_Of_0104_2ptshot_PREP"}
                    ],
                    failure: "R_Of_01_Goto04_MISHANDLE",
                    actionSequence:  {
                        type: "swipe",
                        "direction": "left"
                    }
                },
                {
                    id: "R_Of_0103_Goto07_PREP",
                    success: [
                        { id: "R_Of_0307_Layup_PREP" },
                    ],
                    failure: "R_Of_0103_Goto07_STEAL",
                    actionSequence:  {
                        type: "stroke",
                        shape: "elbow",
                        rotate: ""
                    }
                },
                {
                    id: "R_Of_0103_3ptshot_PREP",
                    success: [
                        { id: "R_Of_0103_3ptshot_SCORE"}
                    ],
                    failure: "R_Of_0103_3ptshot_BLOCK",
                    actionSequence:  {
                        type: "stroke",
                        shape: "n",
                        rotate: ""
                    }
                },
                {
                    id: "R_Of_0104_Goto06_PREP",
                    success: [
                        { id: "R_Of_0406_Layup_PREP" }
                    ],
                    failure: "R_Of_0104_Goto06_MISHANDLE",
                    actionSequence:  {
                        type: "swipe",
                        "direction": "left"
                    }
                },
                {
                    id: "R_Of_0104_2ptshot_PREP",
                    success: [
                        { id: "R_Of_0104_2ptshot_SCORE"}
                    ],
                    failure: "R_Of_0104_2ptshot_BLOCK",
                    actionSequence:  {
                        type: "swipe",
                        "direction": "up"
                    }
                },
                {
                    id: "R_Of_0104_Layup_PREP",
                    success: [
                        { id: "R_Of_0104_Layup_SCORE"}
                    ],
                    failure: "R_Of_0104_Layup_BLOCK",
                    actionSequence:  {
                        type: "stroke",
                        shape: "elbow",
                        rotate: "cw"
                    }
                },
                {
                    id: "R_Of_0406_Layup_PREP",
                    success: [
                        { id: "R_Of_0406_Layup_SCORE"}
                    ],
                    failure: "R_Of_0406_Layup_BLOCK",
                    actionSequence:  {
                        type: "stroke",
                        shape: "elbow",
                        rotate: "cww"
                    }
                },
                {
                    id: "R_Of_0307_Layup_PREP",
                    success: [
                        { id: "R_Of_0307_Layup_SCORE"}
                    ],
                    failure: "R_Of_0307_Layup_BLOCK",
                    actionSequence:  {
                        type: "stroke",
                        shape: "elbow",
                        rotate: "cw"
                    }
                },
                { id: "R_Of_01_Goto03_MISHANDLE" },
                { id: "R_Of_01_Goto04_MISHANDLE" },
                { id: "R_Of_0103_Goto07_STEAL" },
                { id: "R_Of_0103_3ptshot_SCORE", kidScore: 2 },
                { id: "R_Of_0103_3ptshot_BLOCK" },
                { id: "R_Of_0104_Goto06_MISHANDLE" },
                { id: "R_Of_0104_2ptshot_SCORE", kidScore: 1 },
                { id: "R_Of_0104_2ptshot_BLOCK" },
                { id: "R_Of_0104_Layup_SCORE" },
                { id: "R_Of_0104_Layup_BLOCK" },
                { id: "R_Of_0406_Layup_SCORE", kidScore: 1 },
                { id: "R_Of_0406_Layup_BLOCK" },
                { id: "R_Of_0307_Layup_SCORE", kidScore: 1 },
                { id: "R_Of_0307_Layup_BLOCK" },
            ]
        };

        return levelConfig;
    });