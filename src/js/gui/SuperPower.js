define(
    [
        "./../Utils",
        "./Circle",
        "./SpriteAnimation",
        "text!./../../resources/sprites/superpower.json"
    ],

    function (Utils, Circle, SpriteAnimation, SuperPowerSpriteSheet) {



        var SuperPowerGui = function(canvasEl, options){
            var that = this;
            this.currentSegment = 0;

            options = this.options = Utils.extend({
                canvasWidth: 400,
                canvasHeight: 400,
                segments: 16,
                margin: 0.07,
                radius: 98,
                lineWidth: 4,
                fullInactiveLineWidth: 6,
                fullActiveLineWidth: 18,
                activatedMaxLineWidth: 23,
                strokeStyle: 'rgb(225, 32, 76)',
                activeStrokeStyle: 'rgb(255, 255, 255)',
                glowColor: "#FF4A0F",
                glowAmount: 50,
                rotateOffset: Math.PI/2 - 0.04,
                onClick: null
            }, options);

            if (canvasEl) {
                this.canvasEl = canvasEl;
            }
            else{
                this.canvasEl = document.createElement("canvas");
                this.canvasEl.width = this.options.canvasWidth;
                this.canvasEl.height = this.options.canvasHeight;
            }


            if (this.options.onClick){
                this.canvasEl.addEventListener("click",this.options.onClick);
            }

            this.canvasWidth = this.canvasEl.width;
            this.canvasHeight = this.canvasEl.height;

            this.options.centerX = this.canvasEl.width / 2;
            this.options.centerY = this.canvasEl.height / 2;

            this.ctx = this.canvasEl.getContext("2d");

            this.drawables = {};

            this.segments = [];

            for (var x = 0; x < options.segments; x++) {
                var segmentSize = 2 * Math.PI / options.segments;
                this.segments.push({
                    lineWidth: options.fullInactiveLineWidth,
                    strokeStyle: options.strokeStyle,
                    startAngle: x * segmentSize - options.rotateOffset,
                    endAngle: (x + 1) * segmentSize - options.margin - options.rotateOffset,
                    anglePercent: 0,
                    blurOpacity: 0,
                    alpha: 1
                });
            }

            this.currentStep = 0;


            setupBasketAnimation.call(this);

            //setup render function with proper this reference
            var render = function(){
                that.render();
                window.requestAnimationFrame(render);
            };

            render();
        };


        SuperPowerGui.prototype.constructor = SuperPowerGui;

        SuperPowerGui.prototype.render = function() {
            this.ctx.clearRect(0, 0, this.canvasEl.width, this.canvasEl.height);

            //todo disable when inactive
            drawComboGui.call(this);

            for (var prop in this.drawables){
                this.drawables[prop].step();
            }

            this.currentStep++;
        };

        SuperPowerGui.prototype.addActiveSegment = function(index) {
            var that = this;

            this.currentSegment = index;

            var growAnimLength = 0.4;
            var expandAnimLength = 0.4;

            var options = this.options;
            var segments = this.segments;

            segments[index].lineWidth = options.fullActiveLineWidth;

            for (var x = 0; x < index; x++) {
                segments[x].anglePercent = 1;
                segments[x].alpha = 1;
                TweenMax.fromTo(segments[x], growAnimLength,
                    {
                        lineWidth: options.fullInactiveLineWidth,
                        colorProps: {strokeStyle: options.strokeStyle},
                        blurOpacity: 0
                    },
                    {
                        lineWidth: options.fullActiveLineWidth,
                        colorProps: {strokeStyle: options.activeStrokeStyle},
                        blurOpacity: 1,
                        ease: Power3.easeIn
                    });
            }

            segments[index].blurOpacity = 1;
            segments[index].alpha = 1;

            TweenMax.fromTo(segments[index], expandAnimLength, {
                    anglePercent: 0,
                    colorProps: {strokeStyle: options.activeStrokeStyle},
                },
                {
                    anglePercent: 1,
                    colorProps: {strokeStyle: options.activeStrokeStyle},
                    ease: Power0.easeIn,
                    delay: growAnimLength,
                    onComplete: function(){
                        if (that.currentSegment == options.segments - 1){

                            //make sure to only run this once
                            if (!that.superPowerActiveSegmentsTween) {
                                that.runActivatedAnimation();
                            }
                        }
                    }
                });

            //all segments to return to active size
            if (that.currentSegment < options.segments - 1) {
                for (var x = 0; x <= index; x++) {
                    segments[x].alpha = 1;
                    TweenMax.to(segments[x], expandAnimLength,
                        {
                            lineWidth: options.fullInactiveLineWidth,
                            colorProps: {strokeStyle: options.strokeStyle},
                            blurOpacity: 0,
                            ease: Power3.easeIn,
                            delay: growAnimLength + expandAnimLength
                        });
                }
            }

        };

        SuperPowerGui.prototype.superPowerCharged = function(){
            this.basketOnAnimation.setAnimation("on");
            this.basketOnAnimation.alpha = 1;

            this.basketOffAnimation.setAnimation("off", {paused: true});
            this.basketOffAnimation.alpha = 0;
        };

        SuperPowerGui.prototype.superPowerCancelled = function(){
            this.basketOffAnimation.setAnimation("off");
            this.basketOffAnimation.alpha = 1;

            this.basketOnAnimation.setAnimation("on", {paused: true});
            this.basketOnAnimation.alpha = 0;

            this.currentSegment = 0;
        };

        SuperPowerGui.prototype.runActivatedAnimation = function(){
            var animLength = 1;

            var segments = this.segments;

            for (var x = 0; x < this.options.segments; x++) {
                segments[x].anglePercent = 1;
                segments[x].alpha = 1;
            }

            this.superPowerActiveSegmentsTween = TweenMax.fromTo(segments, animLength,
                {
                    lineWidth: this.options.fullActiveLineWidth
                },
                {
                    lineWidth: this.options.activatedMaxLineWidth,
                    yoyo: true,
                    repeat: -1,
                    ease: Power1.easeIn
                });

            this.superPowerActiveBasketballTween = TweenMax.fromTo(this.basketOnAnimation, animLength,
                {
                    height: this.basketOnAnimation.options.height
                },
                {
                    height: this.basketOnAnimation.options.height * 0.95,
                    yoyo: true,
                    repeat: -1,
                    ease: Power1.easeIn
                });
        };


        SuperPowerGui.prototype.discharge = function(options){
            var that = this;
            var animLength = 2;


            //reset state
            this.currentSegment = 0;
            if (this.superPowerActiveBasketballTween) {
                this.superPowerActiveBasketballTween.kill();
                this.superPowerActiveSegmentsTween.kill();

                delete this.superPowerActiveBasketballTween;
                delete this.superPowerActiveSegmentsTween;
            }


            var dischargeGlowCircle = new Circle({
                ctx: this.ctx,
                centerX: this.options.centerX,
                centerY: this.options.centerY,
                radius: 5,
                strokeStyle: "#ce2249",
                blurRadius: 30,
                lineWidth: 30,
                alpha: 1
            });

            var dischargeStripCircle = new Circle({
                ctx: this.ctx,
                centerX: this.options.centerX,
                centerY: this.options.centerY,
                radius: 5,
                strokeStyle: "white",
                blurRadius: 3,
                lineWidth: 4,
                alpha: 1
            });

            this.drawables["dischargeGlowCircle"] = dischargeGlowCircle;
            this.drawables["dischargeStripCircle"] = dischargeStripCircle;

            TweenMax.fromTo(dischargeGlowCircle, animLength,
                {
                    radius: 1
                },
                {
                    radius: 150,
                    ease: Power3.easeOut,
                    onComplete: function(){
                         delete that.drawables["dischargeGlowCircle"];
                    }

                });

            TweenMax.fromTo(dischargeStripCircle, animLength,
                {
                    radius: 1,
                    blurRadius: 20,
                },
                {
                    radius: 150,
                    blurRadius: 0,
                    ease: Power3.easeOut,
                    onComplete: function(){
                        delete that.drawables["dischargeStripCircle"];

                        if (that.options.onComplete){
                            that.options.onComplete();
                        }
                    }

                });

            this.basketOnAnimation.setAnimation("on", { paused: true});

            //alpha
            TweenMax.fromTo([dischargeGlowCircle, dischargeStripCircle], animLength - animLength * 0.5,
                { alpha: 1 },
                {
                    alpha: 0,
                    delay: animLength - animLength * 0.5,
                    ease: Power3.easeOut
                });

            TweenMax.fromTo(this.basketOffAnimation, animLength * 0.5,
                { alpha: 1 },
                {
                    alpha: 0,
                    ease: Power3.easeOut
                });

            TweenMax.fromTo(this.basketOnAnimation, animLength * 0.5,
                { alpha: 0 },
                {
                    alpha: 1,
                    ease: Power3.easeOut
                });


            //segments

            TweenMax.to(this.segments, animLength * 0.3,
                {
                    alpha: 0,
                    ease: Power0.easeOut
                });
        };

        function setupBasketAnimation(){
            //setup basket animation
            this.basketOffAnimation = new SpriteAnimation({
                ctx: this.ctx,
                spriteSheet: SuperPowerSpriteSheet,
                imageUrl: "/resources/sprites/superpower.png",
                centerX: this.options.centerX + 7,
                centerY: this.options.centerY - 5,
                height: 190
            });

            this.basketOnAnimation = new SpriteAnimation({
                ctx: this.ctx,
                spriteSheet: SuperPowerSpriteSheet,
                imageUrl: "/resources/sprites/superpower.png",
                centerX: this.options.centerX + 7,
                centerY: this.options.centerY - 5,
                height: 190
            });


            this.basketOnAnimation.addAnimation("on","gil est temp0000.png", "gil est temp0031.png");
            this.basketOffAnimation.addAnimation("off","gil est temp0032.png", "gil est temp0060.png");

            this.basketOffAnimation.setAnimation("off", {paused: true});
            this.basketOffAnimation.alpha = 0;

            this.basketOnAnimation.setAnimation("on", {paused: true});
            this.basketOnAnimation.alpha = 1;

            this.drawables["basketOnAnimation"] = this.basketOnAnimation;
            this.drawables["basketOffAnimation"] = this.basketOffAnimation;
        }

        function drawComboGui() {
            var ctx = this.ctx;
            var options = this.options;
            var segments = this.segments;

            var segmentSize = 2 * Math.PI / options.segments;


            ctx.globalAlpha = 1;

            //draw the placeholder lines
            for (var x = 0; x < options.segments; x++) {
                ctx.beginPath();
                ctx.strokeStyle = "#373739";
                ctx.lineWidth = options.lineWidth;
                ctx.arc(options.centerX,
                    options.centerY,
                    options.radius,
                    x * segmentSize - options.rotateOffset,
                    (x + 1) * segmentSize - options.margin - options.rotateOffset
                );
                ctx.stroke();
            }


            //draw the active segments
            for (var x = 0; x < segments.length; x++) {
                var segment = segments[x];

                var startAngle = segment.startAngle;
                var endAngle = segment.startAngle + (segment.endAngle - segment.startAngle) * segment.anglePercent;
                var middleAngle = startAngle + (endAngle - startAngle) / 2;

                //need to offset because the stroke grows in both directions and not only outwards
                var centerOffsetX = Math.cos(middleAngle) * segment.lineWidth / 2 * 0;
                var centerOffsetY = Math.sin(middleAngle) * segment.lineWidth / 2 * 0;

                ctx.save();
                //draw blur
                if (segment.blurOpacity > 0) {
                    ctx.beginPath();
                    ctx.globalAlpha = segment.blurOpacity * segment.alpha;
                    ctx.globalCompositeOperation = "lighten";
                    ctx.strokeStyle = "white";
                    ctx.lineWidth = segment.lineWidth;
                    ctx.shadowBlur = options.glowAmount;
                    ctx.shadowColor = options.glowColor;
                    ctx.arc(options.centerX + centerOffsetX,
                        options.centerY + centerOffsetY,
                        options.radius + (segment.lineWidth / 2 - 1), //the 1 is for pixel correction
                        startAngle,
                        endAngle
                    );
                    ctx.stroke();
                    ctx.shadowBlur = 0;
                }
                // ctx.globalCompositeOperation = "source-over";
                ctx.restore();

                ctx.save();

                //draw line
                ctx.beginPath();
                ctx.globalAlpha = segment.alpha;
                ctx.lineWidth = segment.lineWidth;
                ctx.strokeStyle = segment.strokeStyle;

                ctx.arc(options.centerX + centerOffsetX,
                    options.centerY + centerOffsetY,
                    options.radius + (segment.lineWidth / 2 - 1), //the 1 is for pixel correction
                    startAngle,
                    endAngle
                );
                ctx.stroke();

                ctx.restore();
            }
        }



        return SuperPowerGui;

    });