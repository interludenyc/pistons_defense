define(
    [
        "./../Utils"
    ],

    function (Utils) {

        var SpriteAnimation = function(options){
            var that = this;

            this.options = Utils.extend({
                centerX: 0,
                centerY: 0,
                width: null,
                height: null,
                alpha: 1
            }, options);

            this.width = this.options.width;
            this.height = this.options.height;

            this.ctx = options.ctx;
            this.spriteSheet = JSON.parse(options.spriteSheet);
            this.frameNames = Object.keys(this.spriteSheet.frames);

            this.currentStep = 0;
            this.animations = {};
            this.currentAnimation = null;

            this.imageLoaded = false;

            this.spriteSheetImage = new Image();

            //callback for when the image has loaded
            this.spriteSheetImage.onload = function(){
                that.imageLoaded = true;
            };

            //initiate image loading
            this.spriteSheetImage.src = options.imageUrl;

        };
        SpriteAnimation.prototype.constructor = SpriteAnimation;

        SpriteAnimation.prototype.step = function(){
            if (this.currentAnimation) {

                var frameData = this.spriteSheet.frames[this.frameNames[this.currentFrame]];
                var targetWidth, targetHeight;

                if (this.width && this.height) {
                    targetWidth = this.width;
                    targetHeight = this.height;
                }
                else if (this.width && !this.height) {
                    targetWidth = this.width;
                    targetHeight = this.width * frameData.sourceSize.h / frameData.sourceSize.w;
                }
                else if (!this.width && this.height) {
                    targetHeight = this.height;
                    targetWidth = this.height * frameData.sourceSize.w / frameData.sourceSize.h;
                }
                else {
                    targetWidth = frameData.sourceSize.w;
                    targetHeight = frameData.sourceSize.h;
                }

                var widthRatio = targetWidth / frameData.sourceSize.w;
                var heightRatio = targetHeight / frameData.sourceSize.h;

                var drawData = {
                    dX: this.options.centerX + widthRatio * (frameData.spriteSourceSize.x - frameData.sourceSize.w / 2),
                    dY: this.options.centerY + heightRatio * (frameData.spriteSourceSize.y - frameData.sourceSize.h / 2),
                    width: frameData.frame.w * widthRatio,
                    height: frameData.frame.h * heightRatio
                };

                this.ctx.save();

                this.ctx.globalAlpha = this.alpha;

                if (this.alpha>0) {
                    this.ctx.drawImage(this.spriteSheetImage,
                        frameData.frame.x, frameData.frame.y, frameData.frame.w, frameData.frame.h,
                        drawData.dX,
                        drawData.dY,
                        drawData.width,
                        drawData.height
                    );
                }

                this.ctx.restore();

                if (this.currentStep % 2 ==0) {
                    if (this.currentFrame == this.animations[this.currentAnimation.name].endFrame) {

                        if (this.currentAnimation.loop) {
                            this.currentFrame = this.animations[this.currentAnimation.name].startFrame;
                        }
                        else{
                            this.isPaused = true;
                        }

                    } else {
                        if (!this.isPaused) {
                            this.currentFrame++;
                        }
                    }
                }

            }
            this.currentStep++;
        };

        SpriteAnimation.prototype.addAnimation = function(name,startFrame,endFrame){
            if (this.frameNames.indexOf(startFrame) == -1){
                console.log("no such frame ", startFrame);
            }

            if (this.frameNames.indexOf(endFrame) == -1){
                console.log("no such frame ", endFrame);
            }

            this.animations[name] = {
                startFrame: this.frameNames.indexOf(startFrame),
                endFrame: this.frameNames.indexOf(endFrame)
            }
        };

        SpriteAnimation.prototype.setAnimation = function(name, options){
            var options = Utils.extend({
                    rate: 1,
                    loop: false,
                    paused: false
                },
            options);

            this.currentAnimation = Utils.extend({
                name: name
            }, options);

            this.currentFrame = this.animations[name].startFrame;

            this.isPaused = options.paused;
        };

        return SpriteAnimation;

    });