define(
    [
        "./Utils"
    ],

    function (Utils) {

        var canvasEl, ctx, canvasRect, canDraw;
        var points = [];
        var startDrawTimestamp;
        var timeStamp;

        var minLineWidth = 0.1;
        var maxLineWidth = 20;
        var shrinkDelay = 0;

        function init(parentEl, player){
            canvasEl = document.createElement("canvas");
            canvasEl.style.width = "100%";
            canvasEl.style.height = "100%";
            canvasEl.style.pointerEvents = "none";
            parentEl.appendChild(canvasEl);

            ctx = canvasEl.getContext('2d');

            recalculateDimensions();

            window.addEventListener('orientationchange', function(){
                setTimeout(recalculateDimensions, 1000);
            });

            player.on("fullscreenchange", function(){
                setTimeout(recalculateDimensions, 1000);
            });

            canvasEl.addEventListener("contextmenu",function(e){
                e.preventDefault();
                e.stopPropagation();
                return false;
            });

            window.requestAnimationFrame(render);


            return canvasEl;
        }

        function recalculateDimensions(){
            var rect= canvasEl.getBoundingClientRect();
            ctx.canvas.width = rect.width;
            ctx.canvas.height = rect.height;

            canvasRect = getCanvasRect(canvasEl);
        }

        function render(renderTimeStamp){
            timeStamp=renderTimeStamp;
            drawPoints();
            window.requestAnimationFrame(render);
        }

        function attacheHammerInstance(hammerInstance){
            hammerInstance.on("hammer.input", onHammerInput);
        }

        function clear(){
            ctx.clearRect(0, 0, canvasRect.width, canvasRect.height);
        }

        function enableDrawing(){
            canDraw = true;
        }

        function disableDrawing(){
            canDraw = false;
        }

        //private functions

        var drawConnectedPoint = function(from, to) {
            ctx.strokeStyle = "white";
            ctx.lineWidth = 8;
            ctx.shadowBlur = 20;
            ctx.shadowColor = "#FF4A0F";
            ctx.beginPath();
            ctx.moveTo(points[from].X, points[from].Y);
            ctx.lineTo(points[to].X, points[to].Y);
            ctx.closePath();
            ctx.stroke();

            ctx.strokeStyle = "white";
            ctx.lineWidth = 8;
            ctx.beginPath();
            ctx.moveTo(points[from].X, points[from].Y);
            ctx.lineTo(points[to].X, points[to].Y);
            ctx.closePath();
            ctx.stroke();

        };

        var drawPoints = function(){
            clear();

            if (points.length>=3) {
                for (var x = 2; x < points.length; x++) {
                    var p0 = points[x];
                    var p1 = points[x - 1];
                    var p2 = points[x - 2];

                    var x0 = (p0.X + p1.X) / 2;
                    var y0 = (p0.Y + p1.Y) / 2;

                    var x1 = (p1.X + p2.X) / 2;
                    var y1 = (p1.Y + p2.Y) / 2;

                    var widthPercentage = 1 - Math.max(0, (timeStamp - p0.timeStamp - shrinkDelay) / 500);
                    var lineWidth = (minLineWidth + (maxLineWidth - minLineWidth) * widthPercentage).clamp(minLineWidth, maxLineWidth);

                    if (lineWidth>1) {
                        ctx.strokeStyle = "white";
                        ctx.lineWidth = lineWidth;
                        ctx.shadowBlur = 20;
                        ctx.shadowColor = "#FF4A0F";
                        // ctx.globalAlpha = 0.1;
                        ctx.beginPath();
                        ctx.moveTo(x0, y0);
                        ctx.quadraticCurveTo(p1.X, p1.Y, x1, y1);
                        ctx.stroke();
                    }
                }

                //todo make it DRY

                for (var x=2; x<points.length; x++){
                    var p0 = points[x];
                    var p1 = points[x-1];
                    var p2 = points[x-2];

                    var x0 = (p0.X + p1.X) / 2;
                    var y0 = (p0.Y + p1.Y) / 2;

                    var x1 = (p1.X + p2.X) / 2;
                    var y1 = (p1.Y + p2.Y) / 2;

                    var widthPercentage = 1 - Math.max(0,(timeStamp - p0.timeStamp - shrinkDelay) / 500 ) ;
                    var lineWidth =  (minLineWidth +  (maxLineWidth-minLineWidth) * widthPercentage).clamp(minLineWidth, maxLineWidth);

                    if (lineWidth>1) {
                        ctx.strokeStyle = "white";
                        ctx.lineWidth = lineWidth;
                        ctx.lineJoin = 'round';
                        ctx.lineCap = 'round';
                        ctx.globalAlpha = 1;
                        ctx.shadowBlur = 0;
                        ctx.beginPath();
                        ctx.moveTo(x0, y0);
                        ctx.quadraticCurveTo(p1.X, p1.Y, x1, y1);
                        ctx.stroke();
                    }
                }

            }


        };


        function onHammerInput(ev){
            if(canDraw) {
                var x = ev.center.x;
                var y = ev.center.y;

                x -= canvasRect.x;
                y -= canvasRect.y - document.body.scrollTop;

                if (ev.isFirst) {
                    points = []; // clear
                    startDrawTimestamp = timeStamp;

                    //        this.ctx.fillRect(x - 4, y - 3, 9, 9);
                    ctx.beginPath();
                    ctx.arc(x, y, 15, 0, 2 * Math.PI, false);
                    ctx.fillStyle = 'rgba(255,255,255,1)';
                    ctx.fill();
                }

                points.push(new Point(x, y, timeStamp)); // append
            }

        }

        function getCanvasRect(canvas)
        {
            var w = canvas.width;
            var h = canvas.height;

            var cx = canvas.offsetLeft;
            var cy = canvas.offsetTop;
            while (canvas.offsetParent != null)
            {
                canvas = canvas.offsetParent;
                cx += canvas.offsetLeft;
                cy += canvas.offsetTop;
            }

            return {x: cx, y: cy, width: w, height: h};
        }

        function Point(x, y, timeStamp) // constructor
        {
            this.X = x;
            this.Y = y;
            this.timeStamp = timeStamp;
        }

        return{
            init: init,
            attacheHammerInstance: attacheHammerInstance,
            clear: clear,
            enableDrawing: enableDrawing,
            disableDrawing: disableDrawing
        }

    }
);