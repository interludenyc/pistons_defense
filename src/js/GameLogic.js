define([
        'js/Utils',
        'js/PathFinder',
        'js/LevelLogic',
        'js/LevelGui',
        'js/KeyboardHandler',
        'config/game'
    ],
    function(Utils, PathFinder, LevelLogic, LevelGui, KeyboardHandler, GameConfig) {
        'use strict';

        var GameLogic = function(options){
            this.player = options.player;

            this.pathFinder = new PathFinder();

            this.levelLogic = new LevelLogic(); //todo fold this into constructor
            this.levelLogic.init({
                player: this.player
            });

            this.levelGui = new LevelGui(this.player, this.levelLogic);
            this.levelGui.on("clickedSuperPower", onClickedSuperPower.bind(this));

            if(!Utils.isMobile()) {
                this.keyboardHandler = new KeyboardHandler(this.levelLogic);
            }


            this.kidScore = 0;
            this.opponentScore = 0;
        };

        GameLogic.prototype.constructor = GameLogic;

        GameLogic.prototype.selectNextPlayer = function(){
            this.currentPlayer = GameConfig.players.reggie;
            this.flowIndex = 0;

            //add the player introduction
            this.player.append(this.currentPlayer.intro);

            this.selectNextFlow();

            //debug
            // this.player.seek(player.playlist[player.playlist.length-1]);
        };

        GameLogic.prototype.selectNextFlow = function(){
            var that = this;

            if (this.flowIndex % 2 == 0  ) {
                this.pathFinder.setLevel(this.currentPlayer.offense);
            }
            else{
                this.pathFinder.setLevel(this.currentPlayer.defense);
            }

            this.levelSteps = that.pathFinder.getSteps();
            // window.levelSteps = levelSteps;
            this.levelNodes = this.pathFinder.getStepNodes(this.levelSteps, player);
            console.log(this.levelNodes.map(function(item){ return item.id}));

            //add the first move node
            this.player.append(this.levelNodes[0]);

            this.levelNodes[0].once("nodestart",function(){
                that.levelGui.show();
            });

            for (var x=0; x<this.levelNodes.length; x++){
                (function() {
                    var levelStep = that.levelSteps[x];
                    var node = that.levelNodes[x];

                    var nextStep = that.levelSteps[x+1];
                    var nextNextStep = that.levelSteps[x+2];
                    var nextNode = that.levelNodes[x+1];

                    // node.once("nodestart", function(node){
                    //     console.log(node.id);
                    // });

                    if(nextStep) {
                        levelStep.success.forEach(function (element, index, array) {
                            node.addPrefetch("pistons_defense." + element.id);
                        });


                        //nodes with actions
                        if (levelStep.actionSequence) {

                            //add failure prefetch
                            node.addPrefetch("pistons_defense."+levelStep.failure);
                            if (levelStep.super){
                                node.addPrefetch("pistons_defense."+levelStep.super);
                            }

                            node.once("nodestart", function(){
                                that.currentStep = levelStep;
                            });

                            node.once("nodeend", function(){
                                that.currentStep = null;
                            });

                            that.levelLogic.attachActionSequence(node, levelStep.actionSequence)
                                .then(function (playerSuccess) {
                                    var failStep = that.pathFinder.getStep(levelStep.failure);
                                    var failNode = that.player.repository.get("pistons_defense."+levelStep.failure);

                                    if (playerSuccess) {
                                        console.log("SUCCESS ON "+node.id);

                                        if (nextNextStep == null){
                                            //last succesful move ends the current flow
                                            attachEndOfFlow.call(that,nextStep);
                                        }
                                    }
                                    else{
                                        //todo handle id != step id
                                        console.log("FAILED ON "+node.id);
                                        nextNode = failNode;
                                        nextStep = failStep;

                                        //failure ends the current flow
                                        attachEndOfFlow.call(that,nextStep);
                                    }

                                    that.player.seek(nextNode);

                                    that.levelGui.hidePrompts();
                                });

                            //append failure for when time elapses
                            node.once("nodecritical", function(node){
                                that.player.append("pistons_defense."+levelStep.failure);
                            });
                        }
                        else {
                            //nodes with no action, in the middle of the flow (like begin nodes)

                            node.once("nodecritical", function(node){
                                // avoid player bug where sometimes nodeCritical gets called twice
                                if (that.player.playlist[that.player.playlist.length-1].id != nextNode.id){
                                    that.player.append(nextNode);

                                    //todo temp test for weights
                                    //make sure you get different paths
                                    var successEdge = levelStep.success.filter(function(item){return item.id == nextStep.id})[0];
                                    // successEdge.weight = successEdge.weight ? successEdge.weight * 0.5: 0.5;
                                }
                            });
                        }

                    }

                })()
            }
        };

        //// private functions

        function onClickedSuperPower(){
            if (this.currentStep && this.currentStep.super &&
                    this.levelLogic.comboGauge == this.levelLogic.maxComboGauge //refactor this
                ){
                //last succesful move ends the current flow
                var nextStep = this.pathFinder.getStep(this.currentStep.super);
                attachEndOfFlow.call(this,nextStep);

                this.player.seek("pistons_defense."+this.currentStep.super);
                this.levelGui.dischargeSuperPower();
                this.levelGui.hidePrompts();

            }
        }

        function attachEndOfFlow(step){
            var that = this;
            //end of flow
            var node = that.player.repository.get("pistons_defense."+step.id);

            //todo change the time
            node.once("timeupdate:2", function(node){
                if (step.kidScore){
                    that.kidScore += step.kidScore;
                    that.levelGui.updateKidScore(that.kidScore);

                }
                else if (step.opponentScore){
                    that.opponentScore += step.opponentScore;
                    that.levelGui.updateOpponentScore(that.opponentScore);
                }
            });

            node.once("nodecritical", function(node){
                // console.log("end of flow");

                that.flowIndex ++;
                if (that.flowIndex == 6){
                    that.levelGui.hide();
                    that.player.append("pistons_defense.Outro_Tests");
                }
                else {
                    that.selectNextFlow();
                }
            });
        }

        return GameLogic;

    });