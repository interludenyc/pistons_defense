// Add tasks here that you would like to override from project-kit.

var gulp = require('gulp');
var runSequence = require('run-sequence').use(gulp);
var umd = require('gulp-umd');

var file_paths = global.file_paths;

// gulp.task('pack:app', function() {} );

var sass = require('gulp-sass');
gulp.task('sass', function () {
    gulp.src('./src/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./src/stylesheets'));
});

gulp.task('umd', function(){
    return gulp.src(['./node_modules/eventemitter3/index.js'], { base: './node_modules' })
        .pipe(umd(
            { exports: function(file){
                    return 'EventEmitter'
                }
            }))
        .pipe(gulp.dest('./dist/node_modules'));
});

gulp.task('pack', false, '', function(done) {
    runSequence(
        'umd',
        'pack:app',
        'tinker-appjs',
        'sass',
        'pack:loader',
        done);
});

gulp.task('copy:misc', false, function () {
    return gulp.src([
        // Copy all files
        file_paths.DIRS.src + '/**/*',

        // Exclude the following files
        // (other tasks will handle the copying of these files)
        '!' + file_paths.DIRS.src + '/config/playerOptions.json',
        '!' + file_paths.DIRS.src + '/config/settings.json',
        '!' + file_paths.DIRS.src + '/resources/images/*/**'

    ], {

        // Include hidden files by default
        dot: true

    }).pipe(gulp.dest(file_paths.DIRS.dist));
});