define([
    './levels/reggieOffense',
    './levels/reggieDefense'
], function (reggieOffense, reggieDefense) {


    var gameConfig = {
        players: {
            reggie: {
                intro: "pistons_defense.REGGIE",
                offense: reggieOffense,
                defense: reggieDefense
            }
        }
    };


    return gameConfig;
});