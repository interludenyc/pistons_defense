define([
    'resources/ivds',
    'resources/nodes',
    'text!config/settings.json',
    'js/Utils',
    'js/GameLogic'

],
function(ivds, nodes, settings,
         Utils, GameLogic) {

    'use strict';

    // app.js variables
    // hold a reference to the Interlude Player
    var player;
    var interlude;

    settings = JSON.parse(settings);

    var gameLogic;

    var head = "pistons_defense.Intro_Flipped_082616";

    // project essentials
    return {
        // the fullowing are expected to be provided for Helix
        settings: settings,

        // Will be called by Helix and is responsible to preare the player
        // for playback of the app (e.g. adding nodes, GUI, etc')
        onInit: function(interludeRef) {
            player = interludeRef.player;
            interlude = interludeRef;

            gameLogic = new GameLogic({
                player: player
            });


            // nodes
            player.repository.add(nodes);

            // Graph
            player.graph.setHead(head);

            if (Utils.isMobile()) {
                document.querySelector("#startscreen").addEventListener("click", function () {
                    if (!player.isFullscreen) {
                        player.requestFullscreen();
                    }
                })
            }
            else{
                player.play();
            }


        },

        // the ID for the head node to be loaded by Helix. This can also be
        // a function.
        head: head,

        // Will be called automatically at the start of the headnode.
        onStarted: function() {
            gameLogic.selectNextPlayer();
        },

        // Will be called by Helix once the project is about to reach its end.
        // Responsible for cleanup.
        onEnd: function() {
            // player.off(null, null, 'demo');
        }
    };
});
