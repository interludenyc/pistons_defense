define([
    "./Utils"
    ],
    function (Utils) {

        var PathFinder = function(){

        };

        PathFinder.prototype.setLevel = function(level){
            //todo implement memory for weight usage in future runs
            this.level = Utils.extend(true,{},level);
        };


        PathFinder.prototype.getSteps = function(){
            var that = this;

            var initialStep = this.getStep(that.level.initialStepId);
            var currentStep = initialStep;
            var nextStep, nextStepId;
            var stepArray = [initialStep];

            while (currentStep){

                if (currentStep.success) {
                    //todo implement weights
                    nextStepId = Utils.getRandomItem(currentStep.success).id;
                    nextStep = this.getStep(nextStepId);
                    if (nextStep) {
                        stepArray.push(nextStep);
                    }
                    currentStep = nextStep;
                }
                else{
                    currentStep = null;
                }
            }

            return stepArray;
        };

        PathFinder.prototype.getStepNodes = function(steps, player){
            var nodes = [];
            var node, step;
            for (var x=0; x<steps.length; x++){
                step = steps[x];
                node = player.repository.get("pistons_defense."+step.id);
                // node.addPrefetch(step.failure);

                nodes.push(node);
            }

            return nodes;
        };

        PathFinder.prototype.getStep = function(id) {
          return this.level.steps.filter(function(d){ return d.id === id })[0]

        };

        return PathFinder;
});
